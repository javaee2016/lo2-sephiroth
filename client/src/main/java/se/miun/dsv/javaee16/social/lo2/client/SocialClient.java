package se.miun.dsv.javaee16.social.lo2.client;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import se.miun.dsv.javaee16.social.lo2.client.parsers.EventsStreamParser;
import se.miun.dsv.javaee16.social.lo2.client.parsers.ParserException;
import se.miun.dsv.javaee16.social.lo2.interfaces.IRemoteFacade;
import se.miun.dsv.javaee16.social.lo2.interfaces.IRemoteQuery;

public class SocialClient {
	
	final private static String defaultUrl = "https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt";
	
	public SocialClient(){};
	
	public void populate() throws ParserException, IOException, NotBoundException{
		
		InputStream is = null;
		try {
			//Parse the graph
			URL inputFileUrl = new URL(defaultUrl);
			is = inputFileUrl.openStream();
			EventsStreamParser esp = new EventsStreamParser(is);
			esp.parseEvents();
			
			Registry registry = LocateRegistry.getRegistry(null);
			IRemoteFacade remoteFacade = (IRemoteFacade)registry.lookup("IRemoteFacade");
			
			remoteFacade.persistAll(esp.getUsers(), esp.getEvents(), esp.getComments());
			
		} catch (IOException | ParserException | NotBoundException e) {
			throw e;
		} finally {
			if(is != null) {
				try {
					is.close();
				} catch(IOException e) {}
			}
		}	
	}
	
	public Object queryJPQL(String jpqlQuery){
		
		Registry registry;
		Object queryResult = null;
		try {
			registry = LocateRegistry.getRegistry(null);
			IRemoteQuery remoteQuery = (IRemoteQuery)registry.lookup("IRemoteQuery");
			queryResult = remoteQuery.queryJPQL(jpqlQuery);
		} catch (RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return queryResult;
		
	}
	
	public Object querySQL(String sqlQuery){
		
		Registry registry;
		Object queryResult = null;
		try {
			registry = LocateRegistry.getRegistry(null);
			IRemoteQuery remoteQuery = (IRemoteQuery)registry.lookup("IRemoteQuery");
			queryResult = remoteQuery.querySQL(sqlQuery);
		} catch (RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return queryResult;
		
	}
	
	public static void main(String[] args) {
		try{
			SocialClient client = new SocialClient();
			client.populate();
		}catch(IOException | ParserException | NotBoundException e){
			e.printStackTrace();
		}
	}
}
