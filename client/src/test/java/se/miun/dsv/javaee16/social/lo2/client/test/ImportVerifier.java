package se.miun.dsv.javaee16.social.lo2.client.test;

import java.io.IOException;
import java.math.BigInteger;
import java.rmi.NotBoundException;
import java.time.LocalDateTime;
import java.util.List;

import se.miun.dsv.javaee16.social.lo2.model.Event;
import se.miun.dsv.javaee16.social.lo2.server.SocialServer;
import se.miun.dsv.javaee16.social.lo2.client.SocialClient;
import se.miun.dsv.javaee16.social.lo2.client.parsers.ParserException;
import se.miun.dsv.javaee16.social.lo2.test.IImportVerifier;

public class ImportVerifier implements IImportVerifier {

	public ImportVerifier() {
	}
	
	public void populateDatabase(){	
		
		SocialServer server = new SocialServer();	
		server.start();
		
		try{
			SocialClient client = new SocialClient();
			client.populate();
		}catch(IOException | ParserException | NotBoundException e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Event> findAllEventsThatOverlapWithOthersJPQL() {
		
		SocialServer server = new SocialServer();
		server.start();
		
		SocialClient client = new SocialClient();
		
		List<Event> events =  
				(List<Event>)client.queryJPQL("SELECT DISTINCT e1 FROM Event e1, Event e2 WHERE e1.startTime < e2.endTime AND e1.endTime > e2.startTime AND e1 <> e2");
		
		return events;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> findFullNamesOfUsersHostingFutureEventsJPQL() {
		
		SocialServer server = new SocialServer();
		server.start();
		
		SocialClient client = new SocialClient();
		
		String queryString = "SELECT DISTINCT CONCAT(u.firstName, ' ', u.lastName) FROM Event e, IN (e.organizers) u WHERE e.startTime > '" + LocalDateTime.now() + "'";
		
		List<String> names = (List<String>)client.queryJPQL(queryString);
		
		return names;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long findNumberOfUsersWithMoreThanOneCommentJPQL() {
		
		SocialServer server = new SocialServer();
		server.start();
		
		SocialClient client = new SocialClient();
		
		List<Long> count = (List<Long>)client.queryJPQL("SELECT count(u) FROM User u WHERE size(u.comments) > 1");
		
		return count.get(0); 
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Event> findPastEventsInHamburgJPQL() {
		
		SocialServer server = new SocialServer();
		server.start();
		
		SocialClient client = new SocialClient();
		
		String queryString = "SELECT e FROM Event e WHERE e.endTime < '" + LocalDateTime.now() + "' AND e.city = 'Hamburg'";
		
		List<Event> events = (List<Event>)client.queryJPQL(queryString);
		
		return events;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> findAllEventsThatOverlapWithOthersSQL(){
		
		SocialServer server = new SocialServer();
		server.start();
		
		SocialClient client = new SocialClient();
		
		List<Object[]> events =  
				(List<Object[]>)client.querySQL("SELECT DISTINCT e1.* FROM Event e1, Event e2 WHERE e1.startTime < e2.endTime AND e1.endTime > e2.startTime AND e1 <> e2");
		
		return events;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> findPastEventsInHamburgSQL(){
		
		SocialServer server = new SocialServer();
		server.start();
		
		SocialClient client = new SocialClient();
		
		String queryString = "SELECT e.title FROM event AS e WHERE e.endTime < '" + LocalDateTime.now() + "' AND e.city = 'Hamburg';";
		
		List<Object> events = (List<Object>)client.querySQL(queryString);
		
		return events;
		
	}

	@SuppressWarnings("unchecked")
	public List<Object> findFullNamesOfUsersHostingFutureEventsSQL(){
		
		SocialServer server = new SocialServer();
		server.start();
		
		SocialClient client = new SocialClient();
		
		String queryString = "SELECT DISTINCT CONCAT(u.firstname, ' ', u.lastname) " +
								"FROM users u " +
								"WHERE u.id IN (SELECT eu.user_id " +
												"FROM events_users eu " +
												"WHERE eu.event_id IN (SELECT e.id " +
																		"FROM event e " +
																		"WHERE starttime > '" + LocalDateTime.now() + "'));";
		
		List<Object> names = (List<Object>)client.querySQL(queryString);
		
		return names;
		
	}

	@SuppressWarnings("unchecked")
	public Integer findNumberOfUsersWithMoreThanOneCommentSQL(){
		
		SocialServer server = new SocialServer();
		server.start();
		
		SocialClient client = new SocialClient();
				
		List<BigInteger> count = (List<BigInteger>)client.querySQL("SELECT count(u) FROM users u WHERE u.id IN (SELECT c.userid FROM comment c GROUP BY userid HAVING count(c.userid) > 1)");
		
		return count.get(0).intValue();
		
	}

}
