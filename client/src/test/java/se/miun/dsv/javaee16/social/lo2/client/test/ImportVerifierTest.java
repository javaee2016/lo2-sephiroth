package se.miun.dsv.javaee16.social.lo2.client.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

import se.miun.dsv.javaee16.social.lo2.model.Event;
import se.miun.dsv.javaee16.social.lo2.test.IImportVerifier;

public class ImportVerifierTest {

	private final IImportVerifier verifier; 
	
	public ImportVerifierTest() {
		this.verifier = new ImportVerifier();
	}
	
	@Test
	public void testPopulateDatabase(){
		
		verifier.populateDatabase();
		assertTrue(true);
		
	}
	
	@Test
	public void testFindAllEventsThatOverlapWithOthersJPQL() {
		List<Event> overlaps = verifier.findAllEventsThatOverlapWithOthersJPQL();

		assertEquals(2, overlaps.size());
		
		overlaps.sort((e1,e2) -> e1.getStartTime().compareTo(e2.getStartTime()));

		Event earlierStartEvent = overlaps.get(0);
		Event laterStartEvent = overlaps.get(0);
		
		assertTrue(laterStartEvent.getStartTime().isBefore(earlierStartEvent.getEndTime()));
	}

	@Test
	public void testFindFullNamesOfUsersHostingFutureEventsJPQL() {
		List<String> futureHosts = verifier.findFullNamesOfUsersHostingFutureEventsJPQL();

		 assertTrue(futureHosts.contains("Robert Jonsson"));
		 assertTrue(futureHosts.contains("Christoffer Fink"));
		 assertTrue(futureHosts.contains("Per Ekeroot"));
		 assertTrue(futureHosts.contains("Felix Dobslaw"));
		 assertTrue(futureHosts.contains("Fredrik Aletind"));
	}

	@Test
	public void testFindNumberOfUsersWithMoreThanThreeCommentsJPQL() {
		long count = verifier.findNumberOfUsersWithMoreThanOneCommentJPQL();
		assertEquals(3L, count);
	}

	@Test
	public void testFindPastEventsInHamburgJPQL() {
		List<Event> events = verifier.findPastEventsInHamburgJPQL();
		assertEquals(2, events.size());
		assertEquals("City Cleaning", events.get(0).getTitle());
	}
	
	@Test
	public void testFindAllEventsThatOverlapWithOthersSQL() {
		List<Object[]> overlaps = verifier.findAllEventsThatOverlapWithOthersSQL();

		assertEquals(2, overlaps.size());	
	}

	@Test
	public void testFindFullNamesOfUsersHostingFutureEventsSQL() {
		List<Object> futureHosts = verifier.findFullNamesOfUsersHostingFutureEventsSQL();
		
		List<String> futureHostNames = new ArrayList<String>();
		
		for(Object name : futureHosts)
		{
			futureHostNames.add((String)name);
		}
		
		 assertTrue(futureHostNames.contains("Robert Jonsson"));
		 assertTrue(futureHostNames.contains("Christoffer Fink"));
		 assertTrue(futureHostNames.contains("Per Ekeroot"));
		 assertTrue(futureHostNames.contains("Felix Dobslaw"));
		 assertTrue(futureHostNames.contains("Fredrik Aletind"));
	}

	@Test
	public void testFindNumberOfUsersWithMoreThanThreeCommentsSQL() {
		int count = verifier.findNumberOfUsersWithMoreThanOneCommentSQL();
		assertEquals(3, count);
	}

	@Test
	public void testFindPastEventsInHamburgSQL() {
		List<Object> events = verifier.findPastEventsInHamburgSQL();
		
		List<String> eventTitles = new ArrayList<String>();
		
		for(Object event : events)
		{
			eventTitles.add((String)event);
		}
		
		assertEquals(2, eventTitles.size());
		assertEquals("City Cleaning", eventTitles.get(0));
	}
}