package se.miun.dsv.javaee16.social.lo2.test;

import java.util.List;

import se.miun.dsv.javaee16.social.lo2.model.Event;

public interface IImportVerifier {
	
	public abstract void populateDatabase();
	
	public abstract List<Event> findAllEventsThatOverlapWithOthersJPQL();

	public abstract List<String> findFullNamesOfUsersHostingFutureEventsJPQL();

	public abstract Long findNumberOfUsersWithMoreThanOneCommentJPQL();

	public abstract List<Event> findPastEventsInHamburgJPQL();
	
	public abstract List<Object[]> findAllEventsThatOverlapWithOthersSQL();

	public abstract List<Object> findFullNamesOfUsersHostingFutureEventsSQL();

	public abstract Integer findNumberOfUsersWithMoreThanOneCommentSQL();

	public abstract List<Object> findPastEventsInHamburgSQL();
	
}
