package se.miun.dsv.javaee16.social.lo2.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Set;

import se.miun.dsv.javaee16.social.lo2.model.Comment;
import se.miun.dsv.javaee16.social.lo2.model.Event;
import se.miun.dsv.javaee16.social.lo2.model.User;

public interface IRemoteFacade extends Remote {
	
	public void persistAll(Set<User> users, Set<Event> events, Set<Comment> comments) throws RemoteException;
	
}
