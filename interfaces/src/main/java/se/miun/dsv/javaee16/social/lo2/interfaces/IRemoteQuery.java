package se.miun.dsv.javaee16.social.lo2.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IRemoteQuery extends Remote {
	
	public Object queryJPQL(String jpqlQuery) throws RemoteException;
	
	public Object querySQL(String sqlQuery) throws RemoteException;
	
}
