package se.miun.dsv.javaee16.social.lo2.server;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import se.miun.dsv.javaee16.social.lo2.model.Comment;
import se.miun.dsv.javaee16.social.lo2.model.Event;
import se.miun.dsv.javaee16.social.lo2.model.User;

public class SocialDAO {
	
	private static EntityManagerFactory entityManagerFactory = Persistence
			.createEntityManagerFactory("social");
	
	
	public void persistUsers(Set<User> users){
		EntityManager manager = entityManagerFactory.createEntityManager();

		EntityTransaction tx = manager.getTransaction();
		try {
				
			tx.begin();

			for(User u : users) { 
				manager.persist(u); 
				}
			
			tx.commit();
				
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		} finally {
			manager.close();
		}
				
	}
	
	public void persistEvents(Set<Event> events){
		EntityManager manager = entityManagerFactory.createEntityManager();

		EntityTransaction tx = manager.getTransaction();
		try {
				
			tx.begin();

			for(Event e : events) { 
				manager.persist(e); 
				}
			
			tx.commit();
				
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		} finally {
			manager.close();
		}
				
	}
	
	public void persistComments(Set<Comment> comments){
		EntityManager manager = entityManagerFactory.createEntityManager();

		EntityTransaction tx = manager.getTransaction();
		try {
				
			tx.begin();

			for(Comment c : comments) { 
				manager.persist(c); 
				}
			
			tx.commit();
				
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		} finally {
			manager.close();
		}
				
	}
	
	public Object queryJPQL(String jpqlQuery){
		EntityManager manager = entityManagerFactory.createEntityManager();
		
		Object result = manager.createQuery(jpqlQuery).getResultList();
		
		manager.close();
		
		return result;
	}
	
	
	public Object querySQL(String sqlQuery){
		EntityManager manager = entityManagerFactory.createEntityManager();
		
		Object result = manager.createNativeQuery(sqlQuery).getResultList();
		
		manager.close();
		
		return result;
	}
	
	public void shutDown() {
		entityManagerFactory.close();
	}
}
