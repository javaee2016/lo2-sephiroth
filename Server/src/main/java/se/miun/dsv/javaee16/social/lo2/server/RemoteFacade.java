package se.miun.dsv.javaee16.social.lo2.server;


import java.rmi.RemoteException;
import java.util.Set;

import se.miun.dsv.javaee16.social.lo2.interfaces.IRemoteFacade;
import se.miun.dsv.javaee16.social.lo2.interfaces.IRemoteQuery;
import se.miun.dsv.javaee16.social.lo2.model.Comment;
import se.miun.dsv.javaee16.social.lo2.model.Event;
import se.miun.dsv.javaee16.social.lo2.model.User;

public class RemoteFacade implements IRemoteFacade, IRemoteQuery {
	
	public void persistAll(Set<User> users, Set<Event> events, Set<Comment> comments){
		
		SocialDAO dao = new SocialDAO();
		
		dao.persistUsers(users);
		dao.persistEvents(events);
		dao.persistComments(comments);
		
	}
	
	public Object queryJPQL(String jpqlQuery){
		
		SocialDAO dao = new SocialDAO();
		
		return dao.queryJPQL(jpqlQuery);
				
	}
	
	public Object querySQL(String sqlQuery){
		
		SocialDAO dao = new SocialDAO();
		
		return dao.querySQL(sqlQuery);		
	}
	
}
