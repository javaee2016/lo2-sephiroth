package se.miun.dsv.javaee16.social.lo2.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import se.miun.dsv.javaee16.social.lo2.interfaces.IRemoteFacade;
import se.miun.dsv.javaee16.social.lo2.interfaces.IRemoteQuery;

public class SocialServer {
	
	public SocialServer(){}
	
	public void start(){
	
		try {
			
			RemoteFacade facade = new RemoteFacade();
			RemoteFacade queryFacade = new RemoteFacade();
			
			IRemoteFacade stub = (IRemoteFacade) UnicastRemoteObject.exportObject(facade, 0);
			IRemoteQuery queryStub = (IRemoteQuery) UnicastRemoteObject.exportObject(queryFacade, 0);
			
			Registry registry = null;
			
			try {
				System.err.println("Attempting to connect to existing registry");
				registry = LocateRegistry.getRegistry(1099);
				//Attempt to provoke a RemoteException in case the registry does not exist, and we need to create our own
				registry.list();
				//If we made it this far, we will continue to use existing registry
				System.err.println("Connected to existing registry");
			} catch(RemoteException e) {
				//Communication with the registry failed, attempt to create own
				registry = LocateRegistry.createRegistry(1099);
				System.err.println("Created new registry");
			}
			
			registry.rebind("IRemoteFacade", stub);
			registry.rebind("IRemoteQuery", queryStub);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public static void main(String[] args) {
		
		SocialServer server = new SocialServer();
		
		server.start();
		
	}

}
